﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpManager : MonoBehaviour
{
    public GameObject[] doors;
    public GameObject[] rewardAreas;
    public GameObject resetArea;
    public int NbActivatedReward { get; private set; }

    private void Reset()
    {
        foreach (var door in doors)
        {
            door.SetActive(false);
        }

        foreach (var reward in rewardAreas)
        {
            reward.SetActive(true);
        }

        NbActivatedReward = rewardAreas.Length;
        resetArea.SetActive(false);
    }

    public void Start()
    {
        Reset();
    }

    public void UpdateReward(GameObject go)
    {
        if(go.CompareTag("ResetArea"))
        {
            Reset();
        }
        else if(go.CompareTag("Reward"))
        {
            go.SetActive(false);
            --NbActivatedReward;
        }
    }
}
