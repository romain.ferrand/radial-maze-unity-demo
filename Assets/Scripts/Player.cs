﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float directionalSpeed;
    public float rotationSpeed;
    public Text scoreText;
    private Rigidbody _playerRbody;
    private ExpManager _expManager;
    private 
    // Start is called before the first frame update
    void Start()
    {
        _playerRbody = GetComponent<Rigidbody>();
        _expManager = GameObject.Find("ExpManager").GetComponent<ExpManager>();
        scoreText.text = $"Rewards left {_expManager.NbActivatedReward}";

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = Vector3.zero;
        Vector3 rotation = Vector3.zero;
        if (Input.GetKey(KeyCode.D)) rotation = transform.up * 1f;
        if (Input.GetKey(KeyCode.A)) rotation = transform.up * -1f;
        if (Input.GetKey(KeyCode.W)) direction = transform.forward * 1f;
        if (Input.GetKey(KeyCode.S)) direction = transform.forward * -1f;
        transform.Rotate(rotation, 
            Time.fixedDeltaTime * rotationSpeed);
        _playerRbody.AddForce(direction * directionalSpeed, ForceMode.VelocityChange);

    }
    private void OnTriggerEnter(Collider other)
    {
        _expManager.UpdateReward(other.gameObject);
        scoreText.text = $"Rewards left {_expManager.NbActivatedReward}";

    }
}
