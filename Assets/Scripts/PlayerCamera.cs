﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
// Start is called before the first frame update
    public GameObject goPlayer;
    public Vector3 offset;

    // Update is called once per frame
    void Update()
    {
        var transform1 = transform;
        transform1.position = goPlayer.transform.position + offset;
        transform1.localEulerAngles = goPlayer.transform.eulerAngles;
        
    }
}
